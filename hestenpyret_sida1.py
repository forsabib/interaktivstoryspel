# coding: iso-8859-15
# This is the starting page.

import pygame
import math
from pygame_helper import *

surface = pygame.display.set_mode((800, 600))  # CR: I moved this out of draw_bg1, since everyone needs the surface
pygame.display.set_caption('h�sten Pyret, s. 1') # Temporary title

bg1 = pygame.image.load('background.png').convert()
priest = pygame.image.load('priestfemaleport.png').convert_alpha()
thief = pygame.image.load('thiefportfem.png').convert_alpha()

def add_text(y, yplus, lines):
    for i in range(0, len(lines)):
        draw_text(surface, lines[i], (100, (y + yplus * i)))

def draw_bg1():
    surface.blit(bg1, (0, 0))
def update():
    pygame.display.update()

def page1():

    draw_bg1()
    
    add_text(100, 30, ["Du ser en vild mustanghingst.",
        "Mustangen n�rmar sig i vild galopp, precis framf�r dig stannar mustangen.", 
        "Du b�st�mmer dig f�r att kalla mustangen..."])

    add_text(200, 30, ["Windy    [Tryck A]", 
        "Vinden                  [Tryck B]", 
        "Dream                   [Tryck c]"])

    update()

    key = filter_key('abc', wait_for_key())
    if key in 'abc':
        return key

    return None

def page2():
    draw_bg1()
#    priestrect = priest.get_rect(topleft=(0, 300))
#    thiefrect = thief.get_rect(topright=(800, 300))
#    surface.blit(priest, priestrect)
#    surface.blit(thief, thiefrect)
    
    add_text(100, 30, ["Windy svarar genast med att v�nda sig i en cirkel.", 
        "Strax d�rp� h�r du m�ngder med hovslag,  i den sekunden f�rst�r du att WIndy �r ledare �ver en flock.", 
        "Flocken best�r av �ver 100 ston och ett 50 tal f�l, men n�r du ser att dem inte �r ensamma utan har s�lskap av en h�stj�gare s� f�r du panik, du best�mmer dig f�r att..."])
    add_text(200, 30, ["Springa genom flocken och skrika. [Tryck a]", 
        "Hoppar upp p� mustangen och litar p� att den ska visa flocken r�tt v�g.[Tryck b]", 
        "springer f�r livet, hoppas att flocken och Windy f�ljer efter. [Tryck c]"])
       
    update()
    
    key = filter_key('abc', wait_for_key())
    if key in 'abc':
        return key
        
"Vinden din nya h�st stannar framf�r dig och l�ter dig stryka hans vackra mule."

"Han frustar och str�cker p� halsen, men n�t �r fel, du h�r r�klukt, Vinden trampar runt p� st�llet och verkar orolig."
"Pl�tsligt ser du eld flamma upp i horisonten, elden sprider sig som om gr�set var en rutchkana."
"Du f�rlitar dig p� Vinden, mustangen som nog kommer att r�dda ditt liv, du..."
"Hoppar upp p� Vinden och h�ller i hans man, du l�ter han l�pa fram f�r hans och ditt liv. [ TRYCK A]"
"Du g�mmer dig bakom en sten. [ TRYCK B]"
"Bara springer. [ TRYCK C]" 

def page3():
    draw_bg1()
    add_text(100, 30, ["Windy svarar genast med att v�nda sig i en cirkel.", 
        "Strax d�rp� h�r du m�ngder med hovslag,  i den sekunden f�rst�r du att WIndy �r ledare �ver en flock.", 
        "Flocken best�r av �ver 100 ston och ett 50 tal f�l, men n�r du ser att dem inte �r ensamma utan har s�lskap av en h�stj�gare s� f�r du panik, du best�mmer dig f�r att..."])
    add_text(200, 30, ["Springa genom flocken och skrika. [Tryck a]", 
        "Hoppar upp p� mustangen och litar p� att den ska visa flocken r�tt v�g.[Tryck b]", 
        "springer f�r livet, hoppas att flocken och Windy f�ljer efter. [Tryck c]"])
       
    update()
    
    key = filter_key('abc', wait_for_key())
    if key in 'abc':
        return key
    return None

