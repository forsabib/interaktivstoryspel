# coding: iso-8859-15
# This is the starting page.

import pygame
import math
from pygame_helper import *

surface = pygame.display.set_mode((800, 600))  # CR: I moved this out of draw_bg1, since everyone needs the surface
pygame.display.set_caption('In the forest, s. 1') # Temporary title

bat = pygame.image.load('bat.png').convert_alpha()
bat2 = pygame.image.load('bat2.png').convert_alpha()
bg1 = pygame.image.load('bgforest.png').convert()
priest = pygame.image.load('priestfemaleport.png').convert_alpha()
thief = pygame.image.load('thiefportfem.png').convert_alpha()

def add_text(y, yplus, lines):
    for i in range(0, len(lines)):
        draw_text(surface, lines[i], (100, (y + yplus * i)))

def draw_bg1():
    surface.blit(bg1, (0, 0))
def update():
    pygame.display.update()

def page1():

    draw_bg1()
    
    add_text(100, 30, ["Du vaknar upp ute i skogen.",
        "R�ster och fotsteg h�rs och n�rmar sig.",
        "Du beslutar dig f�r att..."])

    add_text(200, 30, ["Lugnt f�rbereda dig f�r att h�lsa p� dom. [Tryck a]", 
        "Springa f�r livet! [Tryck b]", 
        "Kl�ttra upp och g�mma dig i ett tr�d. [Tryck c]"])

    oldbatposx = 0
    oldbatposy = 10

    for batposx in range(1, 801):
        batposy = 10 + math.sin(batposx / 4.0) * 3
        batrect = bat.get_rect(topleft=(oldbatposx,oldbatposy))
        surface.blit(bg1, batrect, batrect)
        if batposy > 10:
            surface.blit(bat2, (batposx, batposy))
        else:
            surface.blit(bat, (batposx, batposy))
        batrect = bat.get_rect(topleft=(batposx,batposy))
        oldbatposx = batposx
        oldbatposy = batposy

        key = filter_key('abc', check_for_key())
        if key is not None:
            if key in 'abc':
                return key
            return None
        else:
            update()
            pygame.time.wait(10)

    key = filter_key('abc', wait_for_key())
    if key in 'abc':
        return key

    return None

def page2():
    draw_bg1()
    priestrect = priest.get_rect(topleft=(0, 300))
    thiefrect = thief.get_rect(topright=(800, 300))
    surface.blit(priest, priestrect)
    surface.blit(thief, thiefrect)
    
    add_text(100, 30, ["Tv� unga resande dyker upp. Dom h�lsar tillbaka.", 
        "Den l�nga av dom s�ger: 'Jag �r en skattj�gare, och hon �r en pr�stinna. Vem �r du?'", 
        "Du svarar med att..."])

    add_text(200, 30, ["S�ga att du inte �r n�gon av betydelse. [Tryck a]", 
        "S�ga till dom att du inte kommer ih�g. [Tryck b]", 
        "S�ga att du ocks� �r en skattj�gare, och en pr�stinna. [Tryck c]"])

    update()
    
    key = filter_key('abc', wait_for_key())
    if key in 'abc':
        return key

    return None

