import pygame
import pygame.gfxdraw

pygame.init()
pygame.font.init()

white = (255, 255, 255)
red = (255, 0, 0)
black = (0, 0, 0)

def check_for_key():
    while True:
        k = pygame.event.poll()
        if k.type == pygame.KEYDOWN:
            return k.unicode
        elif k.type == pygame.NOEVENT:
            return None

def wait_for_key():
    while True:
        k = pygame.event.wait()
        if k.type == pygame.KEYDOWN:
            return k.unicode
        
def filter_key(set, key):
    if key is not None:
        key = key.lower()
#        if key in set:
        return key
#    return None
#and later:
#key = filter_key('abc', check_for_key())

font = pygame.font.SysFont(pygame.font.get_default_font(), 20)

def draw_text(surface, text, pos, colour=white):
    s = font.render(text, 1, colour)
    draw_rect = s.get_rect()
    surface.blit(s, draw_rect.move(pos))
