# coding: iso-8859-15
# This is the starting page.

import pygame
import math
from pygame_helper import *

surface = pygame.display.set_mode((800, 600))  # CR: I moved this out of draw_bg1, since everyone needs the surface
pygame.display.set_caption('Pyramid, s. 1') # Temporary title


bg1 = pygame.image.load('pyramid.jpg').convert()
#priest = pygame.image.load('priestfemaleport.png').convert_alpha()
#thief = pygame.image.load('thiefportfem.png').convert_alpha()

def add_text(y, yplus, lines):
    for i in range(0, len(lines)):
        draw_text(surface, lines[i], (100, (y + yplus * i)))

def draw_bg1():
    surface.blit(bg1, (0, 0))
def update():
    pygame.display.update()

def page1():

    draw_bg1()
    
    add_text(100, 30, ["Du vaknar upp ute i �knen.",
        "R�ster och fotsteg h�rs och n�rmar sig.",
        "Du beslutar dig f�r att..."])

    add_text(200, 30, ["Lugnt f�rbereda dig f�r att h�lsa p� dom. [Tryck a]", 
        "Springa f�r livet! [Tryck b]", 
        "gr�va ner dig i sandens. [Tryck c]"])


    update()


    key = filter_key('abc', wait_for_key())
    if key in 'abc':
        return key

    return None

def page2():
    draw_bg1()
#    priestrect = priest.get_rect(topleft=(0, 300))
#    thiefrect = thief.get_rect(topright=(800, 300))
#    surface.blit(priest, priestrect)
#    surface.blit(thief, thiefrect)
    
    add_text(100, 30, ["tre unga resande dyker upp. Dom h�lsar tillbaka.", 
        "Dom tv� l�nga av dom s�ger: 'vi �r tv� krigare, och hon �r en prinsessa. Vem �r du?'", 
        "Du svarar med att..."])

    add_text(200, 30, ["S�ga att du inte �r n�gon av betydelse. [Tryck a]", 
        "s�ga att du �r en bond flicka. [Tryck b]", 
        "S�ga att du ocks� �r en krigare och prinsessa. [Tryck c]"])

    update()
    
    key = filter_key('abc', wait_for_key())
    if key in 'abc':
        return key

    return None

