# Interaktiv berättelse eller spel

Detta är en programmeringsaktivitet på Sörforsa bibliotek. Skapa ditt eget äventyr!

[](Lägg till två mellanslag på slutet av raden för att få en korrekt radbrytning.)

# Medarbetare
Lena Andersson  
creichen  
Anna Mattsson  
Elsa 1 (komplettera med efternamn?)  
Elsa 2 (komplettera med efternamn?)  
Ditt namn?

# Konstnärer vars bilder använts
Stort tack till de konstnärer vars bilder vi använt via Creative Commons-licenser! (https://opengameart.org)  
JAP (Joakim Persson)  
Tamara Ramsay http://vectorgurl.com/  
Heindal_Wesnoth (Mathias Lang)  
Zeldyn (Jessica Cox)  
Julien (© 2012-2013 Julien Jorge <julien.jorge@stuff-o-matic.com>)  
AntumDeluge (Jordan Irwin)  
Ironthunder (Markeus)  
bart (Bart Kelsey)  
Cabbit (Svetlana Kushnariova)  