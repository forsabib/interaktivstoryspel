# Sample for a simple, interactive text adventure, but with prettyish pictures.

import pygame
import math
from pygame_helper import *
from page1 import *

#surface = pygame.display.set_mode((800, 600))  # CR: I moved this out of draw_bg1, since everyone needs the surface
#pygame.display.set_caption('In the forest') # Temporary title

bg2 = pygame.image.load('gateforest.png').convert()
bg3 = pygame.image.load('trees.png').convert()

#def draw_bg1():
#    surface.blit(bg1, (0, 0))
def draw_bg2():
    surface.blit(bg2, (0, 0))
def draw_bg3():
    surface.blit(bg3, (0, 0))

#def update():
#    pygame.display.update()

#Page 1
alt = page1()
priestrect = priest.get_rect(topleft=(0, 300))
thiefrect = thief.get_rect(topright=(800, 300))
if alt == 'a':
    page2()
    #draw_bg1()
    #surface.blit(priest, priestrect)
    #surface.blit(thief, thiefrect)
elif alt == 'b':
    draw_bg2()
elif alt == 'c':
    draw_bg3()
else:
    draw_bg1()
    draw_text(surface, "Bad key.", t1)

update()


#Page 2
wait_for_key()

# Todo:
#0. Make bat move back (slightly lower) and forth, with pauses between appearances.
#1. Add text to the page 2 pages.
#2. Add more pages.
#3. Also clean up.
