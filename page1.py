# This is the starting page.

import pygame
import math
from pygame_helper import *

# Create the `canvas' that we draw to
surface = pygame.display.set_mode((800, 600))
# Sets the window title
pygame.display.set_caption('In the forest, p. 1')

# Loads images that we can use later.
# The `convert_alpha' does two things:
# 1. It transforms the image to match the graphics card configuration, which makes it faster to draw (at the cost of slowing down loading a little)
# 2. It makes sure that the picture's `alpha' channel is preserved, meaning that it retains transparency information
bat = pygame.image.load('bat.png').convert_alpha()
bat2 = pygame.image.load('bat2.png').convert_alpha()
bg1 = pygame.image.load('bgforest.png').convert()
priest = pygame.image.load('priestfemaleport.png').convert_alpha()
thief = pygame.image.load('thiefportfem.png').convert_alpha()

def add_text(y, yplus, lines):
    '''
    Draws text to the screen.
    y     : int              is the y coordinate for the first line of text (0: top of screen, 600: just below screen)
    yplus : int              is the number of pixels that we move down after each line of text
    lines : list of string   is a list of lines of text to print, e.g., ['first line', 'second line']
    '''
    for i in range(0, len(lines)):
        draw_text(surface, lines[i], (100, (y + yplus * i)))

def draw_bg1():
    '''
    Draws the background image bg1.  Consider making this function more general in the future!
    '''
    surface.blit(bg1, (0, 0))

def update():
    '''
    Refreshes the display.  Call tihs when you're finished drawing, and try to avoid calling it while you're in the middle
    of drawing, or the game may flicker or become slow.
    '''
    pygame.display.update()

def page1():
    '''First page of the game.'''

    draw_bg1()

    add_text(100, 30, ["You wake up in a forest.",
        "Voices and accompanying footsteps are approaching.",
        "You decide to..."])

    add_text(200, 30, ["Calmly prepare to greet them. [Press a]", 
        "Run for your life! [Press b]", 
        "Climb up a tree to hide. [Press c]"])

    oldbatposx = 0
    oldbatposy = 10

    for batposx in range(1, 801):
        batposy = 10 + math.sin(batposx / 4.0) * 3
        batrect = bat.get_rect(topleft=(oldbatposx,oldbatposy))
        surface.blit(bg1, batrect, batrect)
        if batposy > 10:
            surface.blit(bat2, (batposx, batposy))
        else:
            surface.blit(bat, (batposx, batposy))
        batrect = bat.get_rect(topleft=(batposx,batposy))
        oldbatposx = batposx
        oldbatposy = batposy

        key = filter_key('abc', check_for_key())
        if key is not None:
            if key in 'abc':
                return key
            return None
        else:
            update()
            pygame.time.wait(10)

    key = filter_key('abc', wait_for_key())
    if key in 'abc':
        return key

    return None

def page2():
    '''Second page of the game.'''

    draw_bg1()
    # The `rect' of an image is the `rectangle' that tells us its size.
    # The `topleft' and `topright' parameters shift this rectangle to a particular place in the screen.
    priestrect = priest.get_rect(topleft=(0, 300))
    thiefrect = thief.get_rect(topright=(800, 300))
    # `blit' draws an image to the target surface, to the specified `rect'.
    surface.blit(priest, priestrect)
    surface.blit(thief, thiefrect)

    add_text(100, 30, ["Two young travellers appear. They return your greeting.",
        "The taller one speaks. 'I'm a treasure hunter, and she's a priestess. Who are you?'",
        "You respond by..."])

    add_text(200, 30, ["Saying that you're no one of consequence. [Press a]", 
        "Telling them you can't quite remember. [Press b]", 
        "Claiming to be a fellow treasure hunter, and priestess too. [Press c]"])

    update()

    key = filter_key('abc', wait_for_key())
    if key in 'abc':
        return key

    return None

